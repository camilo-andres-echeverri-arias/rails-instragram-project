class Comment < ApplicationRecord
  #constants
  ROLES = {
    owner: 1,
    maintainer: 2,
    follower: 3
  }
  belongs_to :user
  belongs_to :post
  before_validate :befor_validatez
  after_validate :after_validatez
  validates :user_id, presence: true
  validates :user, uniqueness: { scope: :post,
        message: "Un usuario solo puede estar asociado una vez por projecto" }
  validates :rol, uniqueness: {scope: :post,
        message: "El proyecto solo puede tener un dueño" }, if: :is_owner?
  def befor_validatez
    binding.pry
    puts 'before'
  end
  def after_validatez
    binding.pry
    puts 'after'
  end
  def is_owner?
    self.rol == ROLES[:owner]
  end
end
